/* global Promise, LockScreenBaseState */

'use strict';

/**
 * This state would guarantee the LockScreen shows the screen.
 */
(function(exports) {

  var LockScreenStatePanelRestore = function() {
    LockScreenBaseState.apply(this, arguments);
  };
  LockScreenStatePanelRestore.prototype =
    Object.create(LockScreenBaseState.prototype);

  LockScreenStatePanelRestore.prototype.start = function(lockScreen) {
    this.type = 'panelRestore';
    this.lockScreen = lockScreen;
    return this;
  };

  LockScreenStatePanelRestore.prototype.transferTo =
  function lsssr_transferTo(inputs) {
    return new Promise((resolve, reject) => {
      resolve();
    });
  };
  exports.LockScreenStatePanelRestore = LockScreenStatePanelRestore;
})(window);

