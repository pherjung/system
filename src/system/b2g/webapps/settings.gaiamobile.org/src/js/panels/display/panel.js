
/* global MozActivity */
/**
 * Wallpaper:
 *   - Select wallpaper by calling wallpaper.selectWallpaper.
 *   - Update wallpaperSrc if wallpaper.image is changed, which is watched
 *     by Observable module.
 * Wallpaper handles only data and does not involve in any UI logic.
 *
 * @module Wallpaper
 */
define('panels/display/wallpaper',['require','shared/settings_listener','shared/settings_url','shared/omadrm/fl','modules/mvvm/observable'],function(require) {
  

  var SettingsListener = require('shared/settings_listener');
  var SettingsURL = require('shared/settings_url');
  var ForwardLock = require('shared/omadrm/fl');
  var Observable = require('modules/mvvm/observable');
  var WALLPAPER_KEY = 'wallpaper.image';
  /**
   * @alias module:display/wallpaper
   * @requires module:modules/mvvm/observable
   * @returns {wallpaperPrototype}
   */
  var wallpaperPrototype = {
    /**
     * Init Wallpaper module.
     *
     * @access private
     * @memberOf wallpaperPrototype
     */
    _init: function w_init() {
      this.WALLPAPER_KEY = WALLPAPER_KEY;
      this.wallpaperURL = new SettingsURL();
      this._watchWallpaperChange();
      this.inSelectWallpaper = false;
    },

    /**
     * Watch the value of wallpaper.image from settings and change wallpaperSrc.
     *
     * @access private
     * @memberOf wallpaperPrototype
     */
    _watchWallpaperChange: function w__watch_wallpaper_change() {
      SettingsListener.observe(this.WALLPAPER_KEY, '',
        function onHomescreenchange(value) {
          this.wallpaperSrc = this.wallpaperURL.set(value);
      }.bind(this));
    },

    /**
     * Switch to wallpaper or gallery app to pick wallpaper.
     *
     * @access private
     * @memberOf wallpaperPrototype
     * @param {String} secret
     */
    _triggerActivity: function w__trigger_activity(secret) {
      this.inSelectWallpaper = true;
      var mozActivity = new MozActivity({
        name: 'pick',
        data: {
          type: ['wallpaper', 'image/*'],
          includeLocked: (secret !== null),
          // XXX: This will not work with Desktop Fx / Simulator.
          width: Math.ceil(window.screen.width * window.devicePixelRatio),
          height: Math.ceil(window.screen.height * window.devicePixelRatio),
          appname: 'setting'
        }
      });
      mozActivity.onsuccess = () => {
        this._onPickSuccess(mozActivity.result.blob, secret);
      };

      mozActivity.onerror = () => {
        this._onPickError();
      }
    },

    /**
     * Call back when picking success.
     *
     * @access private
     * @memberOf wallpaperPrototype
     * @param {String} blob
     * @param {String} secret
     */
    _onPickSuccess: function w__on_pick_success(blob, secret) {
      this.inSelectWallpaper = false;
      if (!blob) {
        return;
      }
      if (blob.type.split('/')[1] === ForwardLock.mimeSubtype) {
        // If this is a locked image from the locked content app, unlock it
        ForwardLock.unlockBlob(secret, blob, function(unlocked) {
          this._setWallpaper(unlocked);
        }.bind(this));
      } else {
        this._setWallpaper(blob);
      }
    },

    /**
     * Update the value of wallpaper.image from settings.
     *
     * @access private
     * @param {String} value
     * @memberOf wallpaperPrototype
     */
    _setWallpaper: function w__set_wallpaper(value) {
      var config = {};
      config[this.WALLPAPER_KEY] = value;
      SettingsListener.getSettingsLock().set(config);
      this._showToast('changessaved');
    },

    _showToast: function w__showToast(msgId) {
      var toast = {
        messageL10nId: msgId,
        latency: 3000,
        useTransition: true
      };
      Toaster.showToast(toast);
    },

    /**
     * Call back when picking fail.
     *
     * @access private
     * @memberOf wallpaperPrototype
     */
    _onPickError: function w__on_pick_error() {
      this.inSelectWallpaper = false;
      console.warn('pick failed!');
    },

    /**
     * Source path of wallpaper.
     *
     * @access public
     * @memberOf wallpaperPrototype
     * @type {String}
     */
    wallpaperSrc: '',

    /**
     * Start to select wallpaper.
     *
     * @access public
     * @memberOf wallpaperPrototype
     */
    selectWallpaper: function w_select_wallpaper() {
      ForwardLock.getKey(this._triggerActivity.bind(this));
    }
  };

  return function ctor_wallpaper() {
    // Create the observable object using the prototype.
    var wallpaper = Observable(wallpaperPrototype);
    wallpaper._init();
    return wallpaper;
  };
});

/**
 * Handle the brightness.
 *
 * @module SliderHandler
 */
define('panels/display/slider_handler',['require','shared/settings_listener'],function(require) {
  
  var SettingsListener = require('shared/settings_listener');
  const BRIGHTNESS_KEY = 'screen.brightness';

  var SliderHandler = function() {
    this._container = null;
    this._element = null;
    this._label = null;
    this._value = null;
  };

  SliderHandler.prototype = {
    init: function sh_init(container) {
      this._container = container;
      this._element = container.querySelector('input');
      this._label = container.querySelector('span.level');

      SettingsListener.observe(BRIGHTNESS_KEY, '', this._setSliderValue.bind(this));
      this._container.addEventListener('keydown',
          this._keydownHandler.bind(this));
    },

    _keydownHandler: function sh_keydownHandler(evt) {
      // Add support to RTL
      let isRtl = window.document.dir === 'rtl';
      let arrowLR = isRtl ? ['ArrowRight', 'ArrowLeft'] : ['ArrowLeft', 'ArrowRight'];

      switch (evt.key) {
        case arrowLR[0]:
          this._setBrightness(this._value <= 10 ? 10 : this._value - 10);
          evt.preventDefault();
          break;

        case arrowLR[1]:
          this._setBrightness(this._value >= 100 ? 100 : this._value + 10);
          evt.preventDefault();
          break;

        default:
          break;
      }
    },

    _setSliderValue: function sh_setSliderValue(value) {
      this._element.value = this._value = value * 100;
      // The slider is transparent if the value is not set yet, display it
      // once the value is set.
      if (this._element.style.opacity !== 1) {
        this._element.style.opacity = 1;
      }
      navigator.mozL10n.setAttributes(this._label,
        'display-percent',
        { percent: this._value });
    },

    _setBrightness: function sh_setBrightness(value) {
      var settingObject = {};
      settingObject[BRIGHTNESS_KEY] = (value === 0) ? 0.1 : value / 100;
      navigator.mozSettings.createLock().set(settingObject);
    }
  };

  return function ctor_sliderHandler() {
    return new SliderHandler();
  };
});

/**
 * The display panel allow user to modify timeout forscreen-off, brightness, and
 * change wallpaper.
 */
define('panels/display/panel',['require','modules/settings_panel','panels/display/wallpaper','panels/display/slider_handler'],function(require) {
  
  var SettingsPanel = require('modules/settings_panel');
  var WallpaperModule = require('panels/display/wallpaper');
  var SliderHandler = require('panels/display/slider_handler');

  return function ctor_display_panel() {
    var wallpaperElements = {};
    var wallpaper = WallpaperModule();
    var brightnessContainer = null;
    var screenTimeoutElement = null;
    var screenTimeoutSelect = null;
    var autoKeypadLockElement = null;
    var autoKeypadLockSelect = null;
    var hideSoftkey = false;
    var listElements = document.querySelectorAll('#display li');

    function _updateSoftkey() {
      var focusedElement = document.querySelector('#display .focus');
      if (hideSoftkey || !focusedElement ||
        focusedElement.classList.contains('none-select')) {
        SettingsSoftkey.hide();
      } else {
        SettingsSoftkey.show();
      }
    }

    function _initFocusEventListener() {
      var i = listElements.length - 1;
      for (i; i >= 0; i--) {
        listElements[i].addEventListener('focus', _updateSoftkey);
      }
    }

    function _removeFocusEventListener() {
      var i = listElements.length - 1;
      for (i; i >= 0; i--) {
        listElements[i].removeEventListener('focus', _updateSoftkey);
      }
    }

    function _initSoftKey() {
      var softkeyParams = {
        menuClassName: 'menu-button',
        header: {
          l10nId: 'message'
        },
        items: [{
          name: 'Select',
          l10nId: 'select',
          priority: 2,
          method: function() {}
        }]
      };
      SettingsSoftkey.init(softkeyParams);
      _updateSoftkey();
    }

    function _wallpaperKeydownHandler(evt) {
      if (evt.key === 'Enter' && !wallpaper.inSelectWallpaper) {
        wallpaper.selectWallpaper();
      }
    }

    function _screenTimeoutHandleKeyDown(evt) {
      switch(evt.key) {
        case 'Accept':
        case 'Enter':
          if (screenTimeoutElement &&
            screenTimeoutElement.classList.contains('focus')) {
            screenTimeoutSelect && screenTimeoutSelect.focus();
          }
          break;
      }
    }

    function _autoLockHandleKeyDown(evt) {
      switch(evt.key) {
        case 'Accept':
        case 'Enter':
          if (autoKeypadLockElement &&
            autoKeypadLockElement.classList.contains('focus')) {
            autoKeypadLockSelect && autoKeypadLockSelect.focus();
          }
          break;
      }
    }
    return SettingsPanel({
      onInit: function dp_onInit(panel) {
        wallpaperElements = {
          wallpaperPreview: panel.querySelector('.wallpaper-preview'),
          wallpaperSelect: panel.querySelector('.wallpaper-select')
        };
        screenTimeoutElement = panel.querySelector('#screen-timeout');
        screenTimeoutSelect = panel.querySelector('#screen-timeout select');
        brightnessContainer = panel.querySelector('.slider-container');
        autoKeypadLockElement = panel.querySelector('#auto-lock');
        autoKeypadLockSelect = panel.querySelector('#auto-lock select');
        var brightness = SliderHandler();
        brightness.init(brightnessContainer);
      },

      onBeforeShow: function dp_onBeforeShow(panel) {
        if (panel.dataset.brightness) {
          hideSoftkey = true;
        }
        _initSoftKey();
        wallpaper.observe('wallpaperSrc', function(newValue) {
          wallpaperElements.wallpaperPreview.src = newValue;
        });
        wallpaperElements.wallpaperPreview.src = wallpaper.wallpaperSrc;
        wallpaperElements.wallpaperSelect.addEventListener('keydown', _wallpaperKeydownHandler);
        screenTimeoutElement.addEventListener('keydown', _screenTimeoutHandleKeyDown);
        autoKeypadLockElement.addEventListener('keydown', _autoLockHandleKeyDown);
        _initFocusEventListener();
      },

      onShow: function dp_onShow(panel) {
        if (panel.dataset.brightness) {
          this._requestFocus(panel, brightnessContainer);
        }
        hideSoftkey = false;
      },

      onBeforeHide: function dp_onBeforeHide() {
        SettingsSoftkey.hide();
        wallpaper.unobserve('wallpaperSrc');
        wallpaperElements.wallpaperSelect.removeEventListener('keydown', _wallpaperKeydownHandler);
        screenTimeoutElement.removeEventListener('keydown', _screenTimeoutHandleKeyDown);
        autoKeypadLockElement.removeEventListener('keydown', _autoLockHandleKeyDown);
        _removeFocusEventListener();
      },

      _requestFocus: function dp_requestFocus(panel, element) {
        var evt = new CustomEvent('panelready', {
          detail: {
            current: '#' + panel.id,
            needFocused: element
          }
        });
        window.dispatchEvent(evt);
      }
    });
  };
});
