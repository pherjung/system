/* © 2017 KAI OS TECHNOLOGIES (HONG KONG) LIMITED All rights reserved.
 * This file or any portion thereof may not be reproduced or used in any manner
 * whatsoever without the express written permission of KAI OS TECHNOLOGIES
 * (HONG KONG) LIMITED. KaiOS is the trademark of KAI OS TECHNOLOGIES (HONG KONG)
 * LIMITED or its affiliate company and may be registered in some jurisdictions.
 * All other trademarks are the property of their respective owners.
 */
// ************************************************************************
// * File Name: fm.js
// * Description: mmitest -> test item: fm radio test.
// * Note:
// ************************************************************************

/* global DEBUG, dump, TestItem */
'use strict';

function debug(s) {
  if (DEBUG) {
    dump('<mmitest> ------: [fm.js] = ' + s + '\n');
  }
}

var FMTest = new TestItem();
var mozFMRadio = navigator.mozFMRadio;
const MIN_FREQ = 87.5;
const MAX_FREQ = 108;

FMTest.updateUI = function() {
  debug('FMTest.updateUI');
  var freq = this.frequency;
  freq = parseFloat(freq.toFixed(1));
  this.centerText.innerHTML = freq + 'MHz.' + '<br>' +
    'Press Left and Right soft-key to change frequency.';

  this.freqP.hidden = false;
  this.freqM.hidden = false;
  this.passButton.disabled = '';
  this.failButton.disabled = '';
};

FMTest.adjustFreq = function(operatorStr) {
  if (operatorStr === 'ArrowLeft' &&
    this.frequency > MIN_FREQ) {
    this.frequency -= 0.1;
  } else if (operatorStr === 'ArrowRight' &&
    this.frequency < MAX_FREQ) {
    this.frequency += 0.1;
  }

  this.updateUI();
  mozFMRadio.setFrequency(this.frequency);
  debug('FMTest: current frequency is ' + parseFloat(this.frequency.toFixed(1)));
}

FMTest.turnOn = function() {
  if (!mozFMRadio.enabled) {
    this.centerText.innerHTML = 'FM init...';
    // Disable both button when FM is initializing.
    this.passButton.disabled = 'disabled';
    this.failButton.disabled = 'disabled';
    mozFMRadio.enable(this.frequency);
    setTimeout(() => {
      this.passButton.disabled = ''
      this.failButton.disabled = '';
    }, 3000);
  }
};

FMTest.turnOff = function() {
  if (mozFMRadio.enabled) {
    mozFMRadio.disable();
  }

  this.freqP.hidden = true;
  this.freqM.hidden = true;
  this.passButton.disabled = 'disabled';
  this.failButton.disabled = '';
  this.centerText.innerHTML = 'Please insert headset.';
};

FMTest.updateFMRadioState = function(acm) {
  if (mozFMRadio.antennaAvailable || acm.headphones) {
    this.turnOn();
  } else {
    this.turnOff();
  }
};

// the following are inherit functions
FMTest.onInit = function() {
  let acm = navigator.mozAudioChannelManager;
  if (mozFMRadio && acm) {
    debug('FMTest onInit ...');
    this.frequency = 103.7;
    this.freqP = document.getElementById('freq+');
    this.freqM = document.getElementById('freq-');
    this.centerText = document.getElementById('centertext');
    this.freqP.hidden = true;
    this.freqM.hidden = true;
    this.passButton.disabled = 'disabled';
    this.failButton.disabled = '';

    // Init the headphone's volume value
    navigator.mozSettings.createLock().set({
      'audio.volume.content': 8
    });

    this.updateFMRadioState(acm);
    mozFMRadio.onenabled = this.updateUI.bind(this);
    acm.onheadphoneschange = this.updateFMRadioState.bind(this, acm);
  }
};

FMTest.onDeinit = function() {
  if (mozFMRadio.enabled) {
    mozFMRadio.disable();
  }
};

FMTest.onHandleEvent = function(evt) {
  switch (evt.key) {
    case 'ArrowLeft':
    case 'ArrowRight':
      if (mozFMRadio.enabled) {
        this.adjustFreq(evt.key);
      }
      break;
    case 'SoftLeft':
      if (this.passButton.disabled) {
        return true;
      }
      break;
    case 'SoftRight':
      if (this.failButton.disabled) {
        return true;
      }
      var event = {
        type: 'click',
        name: 'fail'
      };
      setTimeout(function() {
        if (parent.ManuTest !== undefined) {
          parent.ManuTest.handleEvent.call(parent.ManuTest, event);
        } else {
          parent.AutoTest.handleEvent.call(parent.AutoTest, event);
        }
      }, 800);
      return true;
  }
  return false;
};

window.addEventListener('load', FMTest.init.bind(FMTest));
window.addEventListener('beforeunload', FMTest.uninit.bind(FMTest));
window.addEventListener('keydown', FMTest.handleKeydown.bind(FMTest));
