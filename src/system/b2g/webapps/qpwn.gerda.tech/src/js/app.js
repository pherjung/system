window.addEventListener('DOMContentLoaded', function() {
  
  let actionSelect = document.getElementById('currentsim'),
      actionInput = document.getElementById('action_value')
  
  let ext = navigator.kaiosExtension || navigator.engmodeExtension
  
  function randomizeTTL() {
    return Math.random() > 0.5 ? 64 : 128
  }
  
  function randomizeIMEI() {
    let imei = new Uint8Array(14).map(x=>(Math.random()*1000|0)%10),
        revmap = [0, 2, 4, 6, 8, 1, 3, 5, 7, 9],
        oddsum = imei[0] + imei[2] + imei[4] + imei[6] + imei[8] + imei[10] + imei[12],
        evensum = revmap[imei[1]] + revmap[imei[3]] + revmap[imei[5]] + revmap[imei[7]] + revmap[imei[9]] + revmap[imei[11]] + revmap[imei[13]],
        luhn = 10 - (oddsum + evensum) % 10
    return imei.join('') + (luhn > 9 ? 0 : luhn)
  }
  
  function getCurrentObject() {
    return naviBoard.getActiveElement().querySelector('input, select')
  }
  
  function fireCmd(cmd, successCb, errorCb) {
    var executor = {}
    try {
      executor = ext.startUniversalCommand(cmd, true); 
    }
    catch(e) {
      if(errorCb)
       errorCb(e)
    }
    if(successCb) executor.onsuccess = successCb
    if(errorCb) executor.onerror = errorCb
  }
  
  function reportError(e) {
    window.alert('Command error! ' + e)
  }
  
  function runIMEIupdate(simNumber, newImei, cb) {
    fireCmd('sh /vendor/bin/qpwn.sh ' + simNumber + ' ' + newImei, cb, reportError)
  }
  
  function runTTLupdate(newTTL, cb) {
    fireCmd('echo -n ' + parseInt(newTTL) + ' > /cache/ttl', cb, reportError)
  }
  
  function askAndReboot() {
    if(window.confirm('Changes applied, reboot now?'))
      fireCmd('reboot')
  }
  
  function applyActions(action, val) {
    switch(action) {
      case 1:
      case 2:
        runIMEIupdate(action, val, askAndReboot)
        break;
      case 3:
        runTTLupdate(val, askAndReboot)
        break;
      case 9:
        runIMEIupdate(action, '', askAndReboot)
      default:
        break;
    }
  }
  
  window.addEventListener('keydown', function(e) {
   let actionVal = parseInt(actionSelect.value)
   switch(e.key) {
     case 'SoftRight': //
         applyActions(actionVal, actionInput.value.trim())
       break;
     case 'SoftLeft':
       getCurrentObject().blur()
       if(actionVal === 9)
         actionInput.value = ''
       else if(actionVal === 3) 
         actionInput.value = randomizeTTL()
       else
         actionInput.value = randomizeIMEI()
       break;
     case 'Enter': //run the action
       let obj = getCurrentObject()
       obj.focus()
       break;
   }})
  
  actionSelect.addEventListener('change', function(){
    actionInput.value = ''
  })
  
  naviBoard.setNavigation('qpwn')
})
