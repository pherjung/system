define([], function() {
    var e = {
        toObject: function(e) {
            if (!e) return null;
            var t = {};
            return e.split("&").forEach(function(e) {
                var n = e.split("=");
                t[decodeURIComponent(n[0])] = decodeURIComponent(n[1]);
            }), t;
        },
        fromObject: function(e) {
            var t = "";
            return Object.keys(e).forEach(function(n) {
                t += (t ? "&" : "") + encodeURIComponent(n) + "=" + encodeURIComponent(e[n]);
            }), t;
        }
    };
    return e;
});