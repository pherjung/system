define([ "browserbox", "logic", "./client", "../syncbase", "exports" ], function(e, n, t, o, r) {
    r.probeAccount = function(e, o) {
        var r = n.scope("ImapProber");
        n(r, "connecting", {
            connInfo: o
        });
        var s;
        return t.createImapConnection(e, o, function() {
            n(r, "credentials-updated");
        }).then(function(e) {
            return s = e, n(r, "success"), {
                conn: s
            };
        }).catch(function(e) {
            throw e = t.normalizeImapError(s, e), n(r, "error", {
                error: e
            }), s && s.close(), e;
        });
    };
});