(function(e) {
    function t(e, t, n) {
        return e >= t && n >= e;
    }
    function n(e, t) {
        return Math.floor(e / t);
    }
    function o(e) {
        var t = 0;
        this.get = function() {
            return t >= e.length ? U : Number(e[t]);
        }, this.offset = function(n) {
            if (t += n, 0 > t) throw new Error("Seeking past start of the buffer");
            if (t > e.length) throw new Error("Seeking past EOF");
        }, this.match = function(n) {
            if (n.length > t + e.length) return !1;
            var o;
            for (o = 0; o < n.length; o += 1) if (Number(e[t + o]) !== n[o]) return !1;
            return !0;
        };
    }
    function r(e) {
        var t = 0;
        this.emit = function() {
            var n, o = U;
            for (n = 0; n < arguments.length; ++n) o = Number(arguments[n]), e[t++] = o;
            return o;
        };
    }
    function i(e) {
        var n = 0, o = function() {
            for (var n = [], o = 0, r = e.length; o < e.length; ) {
                var i = e.charCodeAt(o);
                if (t(i, 55296, 57343)) if (t(i, 56320, 57343)) n.push(65533); else if (o === r - 1) n.push(65533); else {
                    var s = e.charCodeAt(o + 1);
                    if (t(s, 56320, 57343)) {
                        var a = 1023 & i, c = 1023 & s;
                        o += 1, n.push(65536 + (a << 10) + c);
                    } else n.push(65533);
                } else n.push(i);
                o += 1;
            }
            return n;
        }();
        this.offset = function(e) {
            if (n += e, 0 > n) throw new Error("Seeking past start of the buffer");
            if (n > o.length) throw new Error("Seeking past EOF");
        }, this.get = function() {
            return n >= o.length ? H : o[n];
        };
    }
    function s() {
        var e = "";
        this.string = function() {
            return e;
        }, this.emit = function(t) {
            65535 >= t ? e += String.fromCharCode(t) : (t -= 65536, e += String.fromCharCode(55296 + (1023 & t >> 10)), 
            e += String.fromCharCode(56320 + (1023 & t)));
        };
    }
    function a(e, t) {
        if (e) throw new Error("EncodingError");
        return t || 65533;
    }
    function c() {
        throw new Error("EncodingError");
    }
    function d(e) {
        if (e = String(e).trim().toLowerCase(), Object.prototype.hasOwnProperty.call(z, e)) return z[e];
        throw new Error("EncodingError: Unknown encoding: " + e);
    }
    function u(e, t) {
        return (t || [])[e] || null;
    }
    function l(e, t) {
        var n = t.indexOf(e);
        return -1 === n ? null : n;
    }
    function h(e) {
        if (e > 39419 && 189e3 > e || e > 1237575) return null;
        var t, n = 0, o = 0, r = W.gb18030;
        for (t = 0; t < r.length; ++t) {
            var i = r[t];
            if (!(i[0] <= e)) break;
            n = i[0], o = i[1];
        }
        return o + e - n;
    }
    function p(e) {
        var t, n = 0, o = 0, r = W.gb18030;
        for (t = 0; t < r.length; ++t) {
            var i = r[t];
            if (!(i[1] <= e)) break;
            n = i[1], o = i[0];
        }
        return o + e - n;
    }
    function f(e) {
        var n = e.fatal, o = 0, r = 0, i = 0, s = 0;
        this.decode = function(e) {
            var c = e.get();
            if (c === U) return 0 !== r ? (r = 0, a(n)) : H;
            if (e.offset(1), 0 === r) {
                if (t(c, 0, 127)) return c;
                if (t(c, 194, 223)) r = 1, s = 128, o = c - 192; else if (t(c, 224, 239)) r = 2, 
                s = 2048, o = c - 224; else {
                    if (!t(c, 240, 244)) return a(n);
                    r = 3, s = 65536, o = c - 240;
                }
                return o *= Math.pow(64, r), null;
            }
            if (!t(c, 128, 191)) return o = 0, r = 0, i = 0, s = 0, e.offset(-1), a(n);
            if (i += 1, o += (c - 128) * Math.pow(64, r - i), i !== r) return null;
            var d = o, u = s;
            return o = 0, r = 0, i = 0, s = 0, t(d, u, 1114111) && !t(d, 55296, 57343) ? d : a(n);
        };
    }
    function m(e) {
        e.fatal, this.encode = function(e, o) {
            var r = o.get();
            if (r === H) return U;
            if (o.offset(1), t(r, 55296, 57343)) return c(r);
            if (t(r, 0, 127)) return e.emit(r);
            var i, s;
            t(r, 128, 2047) ? (i = 1, s = 192) : t(r, 2048, 65535) ? (i = 2, s = 224) : t(r, 65536, 1114111) && (i = 3, 
            s = 240);
            for (var a = e.emit(n(r, Math.pow(64, i)) + s); i > 0; ) {
                var d = n(r, Math.pow(64, i - 1));
                a = e.emit(128 + d % 64), i -= 1;
            }
            return a;
        };
    }
    function g(e, n) {
        var o = n.fatal;
        this.decode = function(n) {
            var r = n.get();
            if (r === U) return H;
            if (n.offset(1), t(r, 0, 127)) return r;
            var i = e[r - 128];
            return null === i ? a(o) : i;
        };
    }
    function y(e, n) {
        n.fatal, this.encode = function(n, o) {
            var r = o.get();
            if (r === H) return U;
            if (o.offset(1), t(r, 0, 127)) return n.emit(r);
            var i = l(r, e);
            return null === i && c(r), n.emit(i + 128);
        };
    }
    function v(e, n) {
        var o = n.fatal, r = 0, i = 0, s = 0;
        this.decode = function(n) {
            var c = n.get();
            if (c === U && 0 === r && 0 === i && 0 === s) return H;
            c !== U || 0 === r && 0 === i && 0 === s || (r = 0, i = 0, s = 0, a(o)), n.offset(1);
            var d;
            if (0 !== s) return d = null, t(c, 48, 57) && (d = h(10 * (126 * (10 * (r - 129) + (i - 48)) + (s - 129)) + c - 48)), 
            r = 0, i = 0, s = 0, null === d ? (n.offset(-3), a(o)) : d;
            if (0 !== i) return t(c, 129, 254) ? (s = c, null) : (n.offset(-2), r = 0, i = 0, 
            a(o));
            if (0 !== r) {
                if (t(c, 48, 57) && e) return i = c, null;
                var l = r, p = null;
                r = 0;
                var f = 127 > c ? 64 : 65;
                return (t(c, 64, 126) || t(c, 128, 254)) && (p = 190 * (l - 129) + (c - f)), d = null === p ? null : u(p, W.gbk), 
                null === p && n.offset(-1), null === d ? a(o) : d;
            }
            return t(c, 0, 127) ? c : 128 === c ? 8364 : t(c, 129, 254) ? (r = c, null) : a(o);
        };
    }
    function _(e, o) {
        o.fatal, this.encode = function(o, r) {
            var i = r.get();
            if (i === H) return U;
            if (r.offset(1), t(i, 0, 127)) return o.emit(i);
            var s = l(i, W.gbk);
            if (null !== s) {
                var a = n(s, 190) + 129, d = s % 190, u = 63 > d ? 64 : 65;
                return o.emit(a, d + u);
            }
            if (null === s && !e) return c(i);
            s = p(i);
            var h = n(n(n(s, 10), 126), 10);
            s -= 10 * 126 * 10 * h;
            var f = n(n(s, 10), 126);
            s -= 126 * 10 * f;
            var m = n(s, 10), g = s - 10 * m;
            return o.emit(h + 129, f + 48, m + 129, g + 48);
        };
    }
    function b(e) {
        var n = e.fatal, o = !1, r = 0;
        this.decode = function(e) {
            var i = e.get();
            if (i === U && 0 === r) return H;
            if (i === U && 0 !== r) return r = 0, a(n);
            if (e.offset(1), 126 === r) return r = 0, 123 === i ? (o = !0, null) : 125 === i ? (o = !1, 
            null) : 126 === i ? 126 : 10 === i ? null : (e.offset(-1), a(n));
            if (0 !== r) {
                var s = r;
                r = 0;
                var c = null;
                return t(i, 33, 126) && (c = u(190 * (s - 1) + (i + 63), W.gbk)), 10 === i && (o = !1), 
                null === c ? a(n) : c;
            }
            return 126 === i ? (r = 126, null) : o ? t(i, 32, 127) ? (r = i, null) : (10 === i && (o = !1), 
            a(n)) : t(i, 0, 127) ? i : a(n);
        };
    }
    function S(e) {
        e.fatal;
        var o = !1;
        this.encode = function(e, r) {
            var i = r.get();
            if (i === H) return U;
            if (r.offset(1), t(i, 0, 127) && o) return r.offset(-1), o = !1, e.emit(126, 125);
            if (126 === i) return e.emit(126, 126);
            if (t(i, 0, 127)) return e.emit(i);
            if (!o) return r.offset(-1), o = !0, e.emit(126, 123);
            var s = l(i, W.gbk);
            if (null === s) return c(i);
            var a = n(s, 190) + 1, d = s % 190 - 63;
            return t(a, 33, 126) && t(d, 33, 126) ? e.emit(a, d) : c(i);
        };
    }
    function T(e) {
        var n = e.fatal, o = 0, r = null;
        this.decode = function(e) {
            if (null !== r) {
                var i = r;
                return r = null, i;
            }
            var s = e.get();
            if (s === U && 0 === o) return H;
            if (s === U && 0 !== o) return o = 0, a(n);
            if (e.offset(1), 0 !== o) {
                var c = o, d = null;
                o = 0;
                var l = 127 > s ? 64 : 98;
                if ((t(s, 64, 126) || t(s, 161, 254)) && (d = 157 * (c - 129) + (s - l)), 1133 === d) return r = 772, 
                202;
                if (1135 === d) return r = 780, 202;
                if (1164 === d) return r = 772, 234;
                if (1166 === d) return r = 780, 234;
                var h = null === d ? null : u(d, W.big5);
                return null === d && e.offset(-1), null === h ? a(n) : h;
            }
            return t(s, 0, 127) ? s : t(s, 129, 254) ? (o = s, null) : a(n);
        };
    }
    function w(e) {
        e.fatal, this.encode = function(e, o) {
            var r = o.get();
            if (r === H) return U;
            if (o.offset(1), t(r, 0, 127)) return e.emit(r);
            var i = l(r, W.big5);
            if (null === i) return c(r);
            var s = n(i, 157) + 129, a = i % 157, d = 63 > a ? 64 : 98;
            return e.emit(s, a + d);
        };
    }
    function I(e) {
        var n = e.fatal, o = 0, r = 0;
        this.decode = function(e) {
            var i = e.get();
            if (i === U) return 0 === o && 0 === r ? H : (o = 0, r = 0, a(n));
            e.offset(1);
            var s, c;
            return 0 !== r ? (s = r, r = 0, c = null, t(s, 161, 254) && t(i, 161, 254) && (c = u(94 * (s - 161) + i - 161, W.jis0212)), 
            t(i, 161, 254) || e.offset(-1), null === c ? a(n) : c) : 142 === o && t(i, 161, 223) ? (o = 0, 
            65377 + i - 161) : 143 === o && t(i, 161, 254) ? (o = 0, r = i, null) : 0 !== o ? (s = o, 
            o = 0, c = null, t(s, 161, 254) && t(i, 161, 254) && (c = u(94 * (s - 161) + i - 161, W.jis0208)), 
            t(i, 161, 254) || e.offset(-1), null === c ? a(n) : c) : t(i, 0, 127) ? i : 142 === i || 143 === i || t(i, 161, 254) ? (o = i, 
            null) : a(n);
        };
    }
    function C(e) {
        e.fatal, this.encode = function(e, o) {
            var r = o.get();
            if (r === H) return U;
            if (o.offset(1), t(r, 0, 127)) return e.emit(r);
            if (165 === r) return e.emit(92);
            if (8254 === r) return e.emit(126);
            if (t(r, 65377, 65439)) return e.emit(142, r - 65377 + 161);
            var i = l(r, W.jis0208);
            if (null === i) return c(r);
            var s = n(i, 94) + 161, a = i % 94 + 161;
            return e.emit(s, a);
        };
    }
    function A(e) {
        var n = e.fatal, o = {
            ASCII: 0,
            escape_start: 1,
            escape_middle: 2,
            escape_final: 3,
            lead: 4,
            trail: 5,
            Katakana: 6
        }, r = o.ASCII, i = !1, s = 0;
        this.decode = function(e) {
            var c = e.get();
            switch (c !== U && e.offset(1), r) {
              default:
              case o.ASCII:
                return 27 === c ? (r = o.escape_start, null) : t(c, 0, 127) ? c : c === U ? H : a(n);

              case o.escape_start:
                return 36 === c || 40 === c ? (s = c, r = o.escape_middle, null) : (c !== U && e.offset(-1), 
                r = o.ASCII, a(n));

              case o.escape_middle:
                var d = s;
                return s = 0, 36 !== d || 64 !== c && 66 !== c ? 36 === d && 40 === c ? (r = o.escape_final, 
                null) : 40 !== d || 66 !== c && 74 !== c ? 40 === d && 73 === c ? (r = o.Katakana, 
                null) : (c === U ? e.offset(-1) : e.offset(-2), r = o.ASCII, a(n)) : (r = o.ASCII, 
                null) : (i = !1, r = o.lead, null);

              case o.escape_final:
                return 68 === c ? (i = !0, r = o.lead, null) : (c === U ? e.offset(-2) : e.offset(-3), 
                r = o.ASCII, a(n));

              case o.lead:
                return 10 === c ? (r = o.ASCII, a(n, 10)) : 27 === c ? (r = o.escape_start, null) : c === U ? H : (s = c, 
                r = o.trail, null);

              case o.trail:
                if (r = o.lead, c === U) return a(n);
                var l = null, h = 94 * (s - 33) + c - 33;
                return t(s, 33, 126) && t(c, 33, 126) && (l = i === !1 ? u(h, W.jis0208) : u(h, W.jis0212)), 
                null === l ? a(n) : l;

              case o.Katakana:
                return 27 === c ? (r = o.escape_start, null) : t(c, 33, 95) ? 65377 + c - 33 : c === U ? H : a(n);
            }
        };
    }
    function E(e) {
        e.fatal;
        var o = {
            ASCII: 0,
            lead: 1,
            Katakana: 2
        }, r = o.ASCII;
        this.encode = function(e, i) {
            var s = i.get();
            if (s === H) return U;
            if (i.offset(1), (t(s, 0, 127) || 165 === s || 8254 === s) && r !== o.ASCII) return i.offset(-1), 
            r = o.ASCII, e.emit(27, 40, 66);
            if (t(s, 0, 127)) return e.emit(s);
            if (165 === s) return e.emit(92);
            if (8254 === s) return e.emit(126);
            if (t(s, 65377, 65439) && r !== o.Katakana) return i.offset(-1), r = o.Katakana, 
            e.emit(27, 40, 73);
            if (t(s, 65377, 65439)) return e.emit(s - 65377 - 33);
            if (r !== o.lead) return i.offset(-1), r = o.lead, e.emit(27, 36, 66);
            var a = l(s, W.jis0208);
            if (null === a) return c(s);
            var d = n(a, 94) + 33, u = a % 94 + 33;
            return e.emit(d, u);
        };
    }
    function M(e) {
        var n = e.fatal, o = 0;
        this.decode = function(e) {
            var r = e.get();
            if (r === U && 0 === o) return H;
            if (r === U && 0 !== o) return o = 0, a(n);
            if (e.offset(1), 0 !== o) {
                var i = o;
                if (o = 0, t(r, 64, 126) || t(r, 128, 252)) {
                    var s = 127 > r ? 64 : 65, c = 160 > i ? 129 : 193, d = u(188 * (i - c) + r - s, W.jis0208);
                    return null === d ? a(n) : d;
                }
                return e.offset(-1), a(n);
            }
            return t(r, 0, 128) ? r : t(r, 161, 223) ? 65377 + r - 161 : t(r, 129, 159) || t(r, 224, 252) ? (o = r, 
            null) : a(n);
        };
    }
    function k(e) {
        e.fatal, this.encode = function(e, o) {
            var r = o.get();
            if (r === H) return U;
            if (o.offset(1), t(r, 0, 128)) return e.emit(r);
            if (165 === r) return e.emit(92);
            if (8254 === r) return e.emit(126);
            if (t(r, 65377, 65439)) return e.emit(r - 65377 + 161);
            var i = l(r, W.jis0208);
            if (null === i) return c(r);
            var s = n(i, 188), a = 31 > s ? 129 : 193, d = i % 188, u = 63 > d ? 64 : 65;
            return e.emit(s + a, d + u);
        };
    }
    function x(e) {
        var n = e.fatal, o = 0;
        this.decode = function(e) {
            var r = e.get();
            if (r === U && 0 === o) return H;
            if (r === U && 0 !== o) return o = 0, a(n);
            if (e.offset(1), 0 !== o) {
                var i = o, s = null;
                if (o = 0, t(i, 129, 198)) {
                    var c = 178 * (i - 129);
                    t(r, 65, 90) ? s = c + r - 65 : t(r, 97, 122) ? s = c + 26 + r - 97 : t(r, 129, 254) && (s = c + 26 + 26 + r - 129);
                }
                t(i, 199, 253) && t(r, 161, 254) && (s = 12460 + 94 * (i - 199) + (r - 161));
                var d = null === s ? null : u(s, W["euc-kr"]);
                return null === s && e.offset(-1), null === d ? a(n) : d;
            }
            return t(r, 0, 127) ? r : t(r, 129, 253) ? (o = r, null) : a(n);
        };
    }
    function D(e) {
        e.fatal, this.encode = function(e, o) {
            var r = o.get();
            if (r === H) return U;
            if (o.offset(1), t(r, 0, 127)) return e.emit(r);
            var i = l(r, W["euc-kr"]);
            if (null === i) return c(r);
            var s, a;
            if (12460 > i) {
                s = n(i, 178) + 129, a = i % 178;
                var d = 26 > a ? 65 : 52 > a ? 71 : 77;
                return e.emit(s, a + d);
            }
            return i -= 12460, s = n(i, 94) + 199, a = i % 94 + 161, e.emit(s, a);
        };
    }
    function N(e) {
        var n = e.fatal, o = {
            ASCII: 0,
            escape_start: 1,
            escape_middle: 2,
            escape_end: 3,
            lead: 4,
            trail: 5
        }, r = o.ASCII, i = 0;
        this.decode = function(e) {
            var s = e.get();
            switch (s !== U && e.offset(1), r) {
              default:
              case o.ASCII:
                return 14 === s ? (r = o.lead, null) : 15 === s ? null : 27 === s ? (r = o.escape_start, 
                null) : t(s, 0, 127) ? s : s === U ? H : a(n);

              case o.escape_start:
                return 36 === s ? (r = o.escape_middle, null) : (s !== U && e.offset(-1), r = o.ASCII, 
                a(n));

              case o.escape_middle:
                return 41 === s ? (r = o.escape_end, null) : (s === U ? e.offset(-1) : e.offset(-2), 
                r = o.ASCII, a(n));

              case o.escape_end:
                return 67 === s ? (r = o.ASCII, null) : (s === U ? e.offset(-2) : e.offset(-3), 
                r = o.ASCII, a(n));

              case o.lead:
                return 10 === s ? (r = o.ASCII, a(n, 10)) : 14 === s ? null : 15 === s ? (r = o.ASCII, 
                null) : s === U ? H : (i = s, r = o.trail, null);

              case o.trail:
                if (r = o.lead, s === U) return a(n);
                var c = null;
                return t(i, 33, 70) && t(s, 33, 126) ? c = u(178 * (i - 1) + 26 + 26 + s - 1, W["euc-kr"]) : t(i, 71, 126) && t(s, 33, 126) && (c = u(12460 + 94 * (i - 71) + (s - 33), W["euc-kr"])), 
                null !== c ? c : a(n);
            }
        };
    }
    function O(e) {
        e.fatal;
        var o = {
            ASCII: 0,
            lead: 1
        }, r = !1, i = o.ASCII;
        this.encode = function(e, s) {
            var a = s.get();
            if (a === H) return U;
            if (r || (r = !0, e.emit(27, 36, 41, 67)), s.offset(1), t(a, 0, 127) && i !== o.ASCII) return s.offset(-1), 
            i = o.ASCII, e.emit(15);
            if (t(a, 0, 127)) return e.emit(a);
            if (i !== o.lead) return s.offset(-1), i = o.lead, e.emit(14);
            var d = l(a, W["euc-kr"]);
            if (null === d) return c(a);
            var u, h;
            return 12460 > d ? (u = n(d, 178) + 1, h = d % 178 - 26 - 26 + 1, t(u, 33, 70) && t(h, 33, 126) ? e.emit(u, h) : c(a)) : (d -= 12460, 
            u = n(d, 94) + 71, h = d % 94 + 33, t(u, 71, 126) && t(h, 33, 126) ? e.emit(u, h) : c(a));
        };
    }
    function R(e, n) {
        var o = n.fatal, r = null, i = null;
        this.decode = function(n) {
            var s = n.get();
            if (s === U && null === r && null === i) return H;
            if (s === U && (null !== r || null !== i)) return a(o);
            if (n.offset(1), null === r) return r = s, null;
            var c;
            if (c = e ? (r << 8) + s : (s << 8) + r, r = null, null !== i) {
                var d = i;
                return i = null, t(c, 56320, 57343) ? 65536 + 1024 * (d - 55296) + (c - 56320) : (n.offset(-2), 
                a(o));
            }
            return t(c, 55296, 56319) ? (i = c, null) : t(c, 56320, 57343) ? a(o) : c;
        };
    }
    function F(e, o) {
        o.fatal, this.encode = function(o, r) {
            function i(t) {
                var n = t >> 8, r = 255 & t;
                return e ? o.emit(n, r) : o.emit(r, n);
            }
            var s = r.get();
            if (s === H) return U;
            if (r.offset(1), t(s, 55296, 57343) && c(s), 65535 >= s) return i(s);
            var a = n(s - 65536, 1024) + 55296, d = (s - 65536) % 1024 + 56320;
            return i(a), i(d);
        };
    }
    function P(e, t) {
        return t.match([ 255, 254 ]) ? (t.offset(2), "utf-16") : t.match([ 254, 255 ]) ? (t.offset(2), 
        "utf-16be") : t.match([ 239, 187, 191 ]) ? (t.offset(3), "utf-8") : e;
    }
    function L(t, n) {
        return this && this !== e ? (t = t ? String(t) : K, n = Object(n), this._encoding = d(t), 
        this._streaming = !1, this._encoder = null, this._options = {
            fatal: Boolean(n.fatal)
        }, Object.defineProperty ? Object.defineProperty(this, "encoding", {
            get: function() {
                return this._encoding.name;
            }
        }) : this.encoding = this._encoding.name, this) : new L(t, n);
    }
    function B(t, n) {
        return this && this !== e ? (t = t ? String(t) : K, n = Object(n), this._encoding = d(t), 
        this._streaming = !1, this._decoder = null, this._options = {
            fatal: Boolean(n.fatal)
        }, Object.defineProperty ? Object.defineProperty(this, "encoding", {
            get: function() {
                return this._encoding.name;
            }
        }) : this.encoding = this._encoding.name, this) : new B(t, n);
    }
    var U = -1, H = -1, q = [ {
        encodings: [ {
            labels: [ "unicode-1-1-utf-8", "utf-8", "utf8" ],
            name: "utf-8"
        } ],
        heading: "The Encoding"
    }, {
        encodings: [ {
            labels: [ "cp864", "ibm864" ],
            name: "ibm864"
        }, {
            labels: [ "cp866", "ibm866" ],
            name: "ibm866"
        }, {
            labels: [ "csisolatin2", "iso-8859-2", "iso-ir-101", "iso8859-2", "iso_8859-2", "l2", "latin2" ],
            name: "iso-8859-2"
        }, {
            labels: [ "csisolatin3", "iso-8859-3", "iso_8859-3", "iso-ir-109", "l3", "latin3" ],
            name: "iso-8859-3"
        }, {
            labels: [ "csisolatin4", "iso-8859-4", "iso_8859-4", "iso-ir-110", "l4", "latin4" ],
            name: "iso-8859-4"
        }, {
            labels: [ "csisolatincyrillic", "cyrillic", "iso-8859-5", "iso_8859-5", "iso-ir-144" ],
            name: "iso-8859-5"
        }, {
            labels: [ "arabic", "csisolatinarabic", "ecma-114", "iso-8859-6", "iso_8859-6", "iso-ir-127" ],
            name: "iso-8859-6"
        }, {
            labels: [ "csisolatingreek", "ecma-118", "elot_928", "greek", "greek8", "iso-8859-7", "iso_8859-7", "iso-ir-126" ],
            name: "iso-8859-7"
        }, {
            labels: [ "csisolatinhebrew", "hebrew", "iso-8859-8", "iso-8859-8-i", "iso-ir-138", "iso_8859-8", "visual" ],
            name: "iso-8859-8"
        }, {
            labels: [ "csisolatin6", "iso-8859-10", "iso-ir-157", "iso8859-10", "l6", "latin6" ],
            name: "iso-8859-10"
        }, {
            labels: [ "iso-8859-13" ],
            name: "iso-8859-13"
        }, {
            labels: [ "iso-8859-14", "iso8859-14" ],
            name: "iso-8859-14"
        }, {
            labels: [ "iso-8859-15", "iso_8859-15" ],
            name: "iso-8859-15"
        }, {
            labels: [ "iso-8859-16" ],
            name: "iso-8859-16"
        }, {
            labels: [ "koi8-r", "koi8_r" ],
            name: "koi8-r"
        }, {
            labels: [ "koi8-u" ],
            name: "koi8-u"
        }, {
            labels: [ "csmacintosh", "mac", "macintosh", "x-mac-roman" ],
            name: "macintosh"
        }, {
            labels: [ "iso-8859-11", "tis-620", "windows-874" ],
            name: "windows-874"
        }, {
            labels: [ "windows-1250", "x-cp1250" ],
            name: "windows-1250"
        }, {
            labels: [ "windows-1251", "x-cp1251" ],
            name: "windows-1251"
        }, {
            labels: [ "ascii", "ansi_x3.4-1968", "csisolatin1", "iso-8859-1", "iso8859-1", "iso_8859-1", "l1", "latin1", "us-ascii", "windows-1252" ],
            name: "windows-1252"
        }, {
            labels: [ "cp1253", "windows-1253" ],
            name: "windows-1253"
        }, {
            labels: [ "csisolatin5", "iso-8859-9", "iso-ir-148", "l5", "latin5", "windows-1254" ],
            name: "windows-1254"
        }, {
            labels: [ "cp1255", "windows-1255" ],
            name: "windows-1255"
        }, {
            labels: [ "cp1256", "windows-1256" ],
            name: "windows-1256"
        }, {
            labels: [ "windows-1257" ],
            name: "windows-1257"
        }, {
            labels: [ "cp1258", "windows-1258" ],
            name: "windows-1258"
        }, {
            labels: [ "x-mac-cyrillic", "x-mac-ukrainian" ],
            name: "x-mac-cyrillic"
        } ],
        heading: "Legacy single-byte encodings"
    }, {
        encodings: [ {
            labels: [ "chinese", "csgb2312", "csiso58gb231280", "gb2312", "gbk", "gb_2312", "gb_2312-80", "iso-ir-58", "x-gbk" ],
            name: "gbk"
        }, {
            labels: [ "gb18030" ],
            name: "gb18030"
        }, {
            labels: [ "hz-gb-2312" ],
            name: "hz-gb-2312"
        } ],
        heading: "Legacy multi-byte Chinese (simplified) encodings"
    }, {
        encodings: [ {
            labels: [ "big5", "big5-hkscs", "cn-big5", "csbig5", "x-x-big5" ],
            name: "big5"
        } ],
        heading: "Legacy multi-byte Chinese (traditional) encodings"
    }, {
        encodings: [ {
            labels: [ "cseucpkdfmtjapanese", "euc-jp", "x-euc-jp" ],
            name: "euc-jp"
        }, {
            labels: [ "csiso2022jp", "iso-2022-jp" ],
            name: "iso-2022-jp"
        }, {
            labels: [ "csshiftjis", "ms_kanji", "shift-jis", "shift_jis", "sjis", "windows-31j", "x-sjis" ],
            name: "shift_jis"
        } ],
        heading: "Legacy multi-byte Japanese encodings"
    }, {
        encodings: [ {
            labels: [ "cseuckr", "csksc56011987", "euc-kr", "iso-ir-149", "korean", "ks_c_5601-1987", "ks_c_5601-1989", "ksc5601", "ksc_5601", "windows-949" ],
            name: "euc-kr"
        }, {
            labels: [ "csiso2022kr", "iso-2022-kr" ],
            name: "iso-2022-kr"
        } ],
        heading: "Legacy multi-byte Korean encodings"
    }, {
        encodings: [ {
            labels: [ "utf-16", "utf-16le" ],
            name: "utf-16"
        }, {
            labels: [ "utf-16be" ],
            name: "utf-16be"
        } ],
        heading: "Legacy utf-16 encodings"
    } ], j = {}, z = {};
    q.forEach(function(e) {
        e.encodings.forEach(function(e) {
            j[e.name] = e, e.labels.forEach(function(t) {
                z[t] = e;
            });
        });
    });
    var W = e["encoding-indexes"] || {};
    j["utf-8"].getEncoder = function(e) {
        return new m(e);
    }, j["utf-8"].getDecoder = function(e) {
        return new f(e);
    }, function() {
        [ "ibm864", "ibm866", "iso-8859-2", "iso-8859-3", "iso-8859-4", "iso-8859-5", "iso-8859-6", "iso-8859-7", "iso-8859-8", "iso-8859-10", "iso-8859-13", "iso-8859-14", "iso-8859-15", "iso-8859-16", "koi8-r", "koi8-u", "macintosh", "windows-874", "windows-1250", "windows-1251", "windows-1252", "windows-1253", "windows-1254", "windows-1255", "windows-1256", "windows-1257", "windows-1258", "x-mac-cyrillic" ].forEach(function(e) {
            var t = j[e], n = W[e];
            t.getDecoder = function(e) {
                return new g(n, e);
            }, t.getEncoder = function(e) {
                return new y(n, e);
            };
        });
    }(), j.gbk.getEncoder = function(e) {
        return new _(!1, e);
    }, j.gbk.getDecoder = function(e) {
        return new v(!1, e);
    }, j.gb18030.getEncoder = function(e) {
        return new _(!0, e);
    }, j.gb18030.getDecoder = function(e) {
        return new v(!0, e);
    }, j["hz-gb-2312"].getEncoder = function(e) {
        return new S(e);
    }, j["hz-gb-2312"].getDecoder = function(e) {
        return new b(e);
    }, j.big5.getEncoder = function(e) {
        return new w(e);
    }, j.big5.getDecoder = function(e) {
        return new T(e);
    }, j["euc-jp"].getEncoder = function(e) {
        return new C(e);
    }, j["euc-jp"].getDecoder = function(e) {
        return new I(e);
    }, j["iso-2022-jp"].getEncoder = function(e) {
        return new E(e);
    }, j["iso-2022-jp"].getDecoder = function(e) {
        return new A(e);
    }, j.shift_jis.getEncoder = function(e) {
        return new k(e);
    }, j.shift_jis.getDecoder = function(e) {
        return new M(e);
    }, j["euc-kr"].getEncoder = function(e) {
        return new D(e);
    }, j["euc-kr"].getDecoder = function(e) {
        return new x(e);
    }, j["iso-2022-kr"].getEncoder = function(e) {
        return new O(e);
    }, j["iso-2022-kr"].getDecoder = function(e) {
        return new N(e);
    }, j["utf-16"].getEncoder = function(e) {
        return new F(!1, e);
    }, j["utf-16"].getDecoder = function(e) {
        return new R(!1, e);
    }, j["utf-16be"].getEncoder = function(e) {
        return new F(!0, e);
    }, j["utf-16be"].getDecoder = function(e) {
        return new R(!0, e);
    };
    var K = "utf-8";
    L.prototype = {
        encode: function(e, t) {
            e = e ? String(e) : "", t = Object(t), this._streaming || (this._encoder = this._encoding.getEncoder(this._options)), 
            this._streaming = Boolean(t.stream);
            for (var n = [], o = new r(n), s = new i(e); s.get() !== H; ) this._encoder.encode(o, s);
            if (!this._streaming) {
                var a;
                do a = this._encoder.encode(o, s); while (a !== U);
                this._encoder = null;
            }
            return new Uint8Array(n);
        }
    }, B.prototype = {
        decode: function(e, t) {
            if (e && !("buffer" in e && "byteOffset" in e && "byteLength" in e)) throw new TypeError("Expected ArrayBufferView");
            e || (e = new Uint8Array(0)), t = Object(t), this._streaming || (this._decoder = this._encoding.getDecoder(this._options)), 
            this._streaming = Boolean(t.stream);
            var n = new Uint8Array(e.buffer, e.byteOffset, e.byteLength), r = new o(n), i = P(this._encoding.name, r);
            if (d(i) !== this._encoding) throw new Error("BOM mismatch");
            for (var a, c = new s(); r.get() !== U; ) a = this._decoder.decode(r), null !== a && a !== H && c.emit(a);
            if (!this._streaming) {
                do a = this._decoder.decode(r), null !== a && a !== H && c.emit(a); while (a !== H);
                this._decoder = null;
            }
            return c.string();
        }
    }, e.TextEncoder = e.TextEncoder || L, e.TextDecoder = e.TextDecoder || B;
})(this);