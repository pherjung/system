'use strict';

/* global CallLog, CallLogDBManager, Contacts, ContactsButtons, LazyLoader,
          MozActivity, SimplePhoneMatcher, Utils */

(function(exports) {
  var currentGroup;
  var detailsButton;
  var addToContactButton;
  var createContactButton;
  var callInfoView;
  var phoneDetailsElt;
  var emailDetailsElt;
  var listDetailsElt;
  var currentScrollPosition = 0;
  var scrollStep = 43;
  var blobURL;

  function updateViewIfNeeded(evt) {
    if (evt.detail.group.id !== currentGroup.id) {
      return;
    }
    updateView(evt.detail.group);
  }

  function updateView(group) {
    currentGroup = group;
    updateGroupInformation(group);
    updateCallDurations(group);
    updatePhoto(group);
    //updateActionButtons(group);
    callInfoView.hidden = false;
    callInfoView.setAttribute('tabindex', 1);
    callInfoView.focus();
  }

  function updatePhoto(group) {
    var ico = document.getElementById('call-info-ico');
    ico.classList.add('default');
    if (!group.voicemail && !group.emergency &&
      group.contact && group.contact.photo) {
      PhotoResize.resize(group.contact.photo).then(blob => {
        blobURL = URL.createObjectURL(blob)
        ico.style.backgroundImage = 'url(' + blobURL + ')';
        ico.classList.remove('default');
      });
    } else {
      ico.removeAttribute('style');
    }
  }

  function isMissedCall(group) {
    return (group.type === 'incoming' || group.type === 'videoCallIncoming') &&
      group.status !== 'connected';
  }

  function getTimeContent (dateTime) {
    var date = new Date(dateTime);
    var _ = navigator.mozL10n.get;
    var f = new navigator.mozL10n.DateTimeFormat();
    var timeFormat = window.navigator.mozHour12 ?
      _('shortTimeFormat12') : _('shortTimeFormat24');
    var formatted = f.localeFormat(date, timeFormat);
    return formatted;
  }

  // group call list by day
  function getGroupedCall (callList) {
    var result = {};
    callList = callList || [];
    callList.map(function (item) {
      if (!result['_day_' + getFormattedDate(parseInt(item.date))]) {
        result['_day_' + getFormattedDate(parseInt(item.date))] = [];
      }
      result['_day_' + getFormattedDate(parseInt(item.date))].push(item);
    });
    return result;
  }

  function getFormattedDate(time) {
    var _ = navigator.mozL10n.get;
    var dtf = new navigator.mozL10n.DateTimeFormat();
    var nowTime = Utils.getDayDate(Date.now());
    var dateTime = Utils.getDayDate(time);
    var day_diff = Math.floor((nowTime - dateTime) / 86400000);
    var formattedTime;
    if (isNaN(day_diff)) {
      formattedTime = _('incorrectDate');
    } else if (!day_diff) {
      formattedTime = _('today');
    } else if (day_diff === 1) {
      formattedTime = _('yesterday');
    } else if (day_diff < 7 && day_diff >= 0) {
      formattedTime = dtf.localeFormat(new Date(time), '%A');
    } else {
      formattedTime = dtf.localeFormat(new Date(time), _('dateTimeFormat_%x'));
    }
    return formattedTime;
  }

  function getDuration(duration) {
    var elapsed = new Date(duration);
    var time = [elapsed.getUTCHours(), elapsed.getUTCMinutes(), elapsed.getUTCSeconds()];
    var timeStrings = [navigator.mozL10n.get('hours'), navigator.mozL10n.get('minutes'), navigator.mozL10n.get('seconds')];
    var timeString = '';
    for (var i = 0; i < time.length; i++) {
      if (time[i] !== 0) {
        timeString += (time[i] + ' ' + timeStrings[i] + ' ');
      }
    }
    return timeString;
  }

  function getTimeListView (group,callList) {
    var res = '';
    var callInfoType;
    var callInfoL10nId;
    if (isMissedCall(group)) {
      callInfoType = 'missed';
      callInfoL10nId = 'missedCall';
    } else if ((group.type === 'incoming' ||
      group.type === 'videoCallIncoming') && group.status === 'connected') {
      callInfoType = 'incoming';
      callInfoL10nId = 'incoming-call';
    } else if (group.type === 'dialing' || group.type === 'videoCallDialing') {
      callInfoType = "outgoing";
      callInfoL10nId = 'outgoing-call';
    }

    var simNum = '';
    var iconData = '';
    var isDS = navigator.mozMobileConnections.length > 1;
    callList = callList || [];
    callList.map(function (item) {
      if (isDS && typeof item.serviceId !== 'undefined') {
        simNum = `-sim${parseInt(item.serviceId) + 1}`;
      }
      if (group.type === 'videoCallIncoming' ||
        group.type === 'videoCallDialing') {
        iconData = `video-call-${callInfoType}${simNum}`;
      } else {
        iconData = `call-${callInfoType}${simNum}`;
      }
      res += '<div><aside class="icon call-type-icon"' + 'data-icon=' +
        iconData + '></aside><div class="call-info-time">' +
        getTimeContent(item.date) + '</div><div class="call-info-type" data-l10n-id="' +
        callInfoL10nId + '"></div> <div class="call-info-duration">' +
        getDuration(item.duration) + '</div></div>';
    });
    return res;
  }

  function renderCallInfo (group) {
    var callInfoList = group.calls;
    var t = document.querySelector('#call-info-template'),
        insertArea = document.querySelector('#call-info-list'),
        dayArea = document.querySelector('.call-info-day'),
        timesListArea = t.content.querySelector('.call-info-time-list'),
        clone,
        groupedCallInfoList = getGroupedCall(callInfoList);
    insertArea.innerHTML = '';
    for (var dayGroup in groupedCallInfoList) if (groupedCallInfoList.hasOwnProperty(dayGroup)){
      var timeList = groupedCallInfoList[dayGroup];
      dayArea.textContent = getFormattedDate(timeList[0].date);
      timesListArea.innerHTML = getTimeListView(group,timeList);
      clone = document.importNode(t.content, true);
      insertArea.appendChild(clone);
    }
  }

  function updateGroupInformation(group) {
    var clientInfo = group.contact || {};
    var clientNum = group.number;

    var elNum = document.getElementById('call-info-number');
    var elCon = document.getElementById('call-info-contact');
    elNum.textContent = clientNum;
    group.voicemail = Voicemail.check(group.number);
    if (group.emergency) {
      elCon.setAttribute('data-l10n-id', 'emergencyNumber');
    } else if (group.voicemail) {
      elCon.setAttribute('data-l10n-id', 'voiceMail');
    } else if (clientInfo.primaryInfo) {
      if (clientInfo.matchingTel) {
        var type = clientInfo.matchingTel.type;
        elNum.textContent = navigator.mozL10n.get(type) + ', ' + clientNum;
      } else {
        elNum.textContent = clientNum;
      }
      elCon.textContent = clientInfo.primaryInfo;
    } else {
      elNum.setAttribute('data-l10n-id', 'unknown');
      elCon.textContent = clientNum;
    }
  }

  function updateCallDurations(group) {
    /* Old groups did not record call durations */
    if (!group.calls) {
      return;
    }
    renderCallInfo(group);
  }

  function updateStartTimes() {
    var startTimeElts = document.querySelectorAll('.js-ci-start-times');
    for (var i=0, il=startTimeElts.length; i<il; i++) {
      var startTimeElt = startTimeElts[i];
      var date = parseInt(startTimeElt.dataset.date, 10);
      startTimeElt.textContent = Utils.prettyDate(date);
    }
  }

  function renderPhones(group, contact) {
    var contactTels = contact.tel.map(function(tel) {
      return tel.value;
    });

    if (!contactTels.some((tel) => tel != null)) {
      return;
    }

    LazyLoader.load(['/shared/js/simple_phone_matcher.js'], function() {
      ContactsButtons.renderPhones(contact);

      // Highlight the contact number that the call info page was opened for,
      // with a color depending on whether the call was missed or not.
      var remark = isMissedCall(group) ? 'remark-missed' : 'remark';

      var groupTel = SimplePhoneMatcher.sanitizedNumber(
        group.number || group.contact.matchingTel.number);
      var groupTelVariants = SimplePhoneMatcher.generateVariants(groupTel);

      // SimplePhoneMatch expects a series of contacts, so we pass it an array
      // containing only the relevant contact.
      contactTels = [contactTels];

      var matchingTels = SimplePhoneMatcher.bestMatch(groupTelVariants,
                                                      contactTels);
      var matchingTel = {value: ''};
      if (matchingTels.totalMatchNum) {
        matchingTel = contact.tel[matchingTels.allMatches[0][0]];
      }
      ContactsButtons.reMark('tel', matchingTel.value, remark);
    });
  }

  function updateActionButtons(group) {
    listDetailsElt.innerHTML = '';

    detailsButton.hidden = true;
    addToContactButton.hidden = true;
    createContactButton.hidden = true;

    if (group.contact) {
      detailsButton.hidden = false;

      Contacts.findByNumber(group.contact.matchingTel.number,
      function(contact, matchingTel) {
        ContactsButtons.renderEmails(contact);
        renderPhones(group, contact);
      });
    } else {
      addToContactButton.hidden = false;
      createContactButton.hidden = false;

      var contact = {
        tel: [
          {
            value: group.number,
            type: 'mobile'
          }
        ]
      };
      renderPhones(group, contact);
    }
  }

  function turnDown () {
    var scrollList = document.querySelector('.call-duration-list');
    var style = window.getComputedStyle(scrollList, null);
    var height = parseInt((style.getPropertyValue('height')).replace(/[^0-9\.]+/g, ''));
    if (scrollList.scrollHeight > 0 && currentScrollPosition + height <= scrollList.scrollHeight) {
      currentScrollPosition += scrollStep;
      scrollList.scrollTop = currentScrollPosition;
    }
  }

  function turnUp (isReset) {
    var scrollList = document.querySelector('.call-duration-list');
    if (currentScrollPosition > 0) {
      currentScrollPosition -= scrollStep;
      scrollList.scrollTop = currentScrollPosition;
    }
    if (isReset === true) {
      currentScrollPosition = 0;
      scrollList.scrollTop = currentScrollPosition;
    }
  }

  function keyEventHandler (evt) {
    console.log('that listener work from call info');
    evt.stopPropagation();
    switch(evt.key) {
      case 'ArrowDown':
        turnDown();
      break;
      case 'ArrowUp':
        turnUp();
      break;
    }
  }

  function close(evt) {
    turnUp(true);
    if (evt && evt.detail && evt.detail.type !== 'back') {
      return;
    }
    window.removeEventListener('CallLogDbNewCall', updateViewIfNeeded);
    window.removeEventListener('keydown', keyEventHandler);
    callInfoView.hidden = true;
    if (blobURL) {
      URL.revokeObjectURL(blobURL);
      blobURL = null;
    }
  }

  function initListeners() {
    window.addEventListener('timeformatchange', updateStartTimes);
  }

  // FIXME/bug 1060290: The build system doesn't allow nested fragments, so we
  // can work around this for now by loading all elements at the root level and
  // then resolving them to the correct location, e.g. fragment-name-stub, on
  // runtime.
  function replaceFragmentStub(stub, fragment) {
    var parent = stub.parentNode;
    parent.insertBefore(fragment, stub);
    parent.removeChild(stub);
    fragment.hidden = false;
  }

  function initFragments() {
    var phoneDetailsStub = document.getElementById('phone-details-stub');
    phoneDetailsElt = document.getElementById('phone-details');

    var emailDetailsStub = document.getElementById('email-details-stub');
    emailDetailsElt = document.getElementById('email-details');

    var contactDetailsElt = document.getElementById('contact-detail');
    listDetailsElt = document.getElementById('call-info-list-details');

    LazyLoader.load([phoneDetailsElt, emailDetailsElt], function() {
      replaceFragmentStub(phoneDetailsStub, phoneDetailsElt);
      replaceFragmentStub(emailDetailsStub, emailDetailsElt);

      ContactsButtons.init(listDetailsElt, contactDetailsElt);
    });
  }

  var CallInfo = {
    _initialised: false,
    show: function(number, date, type, status, id) {
      callInfoView = document.getElementById('call-info-view');
      var self = this;
      LazyLoader.load([callInfoView,
                       '/shared/js/dialer/contacts.js',
                       '/dialer/js/utils.js',
                       '/shared/js/contacts/contacts_buttons.js',
                       '/shared/js/contacts/utilities/templates.js',
                       '/shared/js/contacts/sms_integration.js',
                       '/shared/js/text_normalizer.js',
                       '/shared/js/dialer/telephony_helper.js',
                       '/shared/style/contacts/contacts_buttons.css',
                       '/shared/style/contacts.css',
                       '/dialer/style/buttons.css'], function() {
        if (!self._initialised) {
          self._initialised = true;
          initListeners();
        }
        date = parseInt(date, 10);
        CallLogDBManager.getGroupById(number, date, type, status, id)
          .then(updateView);
        window.addEventListener('CallLogDbNewCall', updateViewIfNeeded);
        window.addEventListener('keydown', keyEventHandler);
        OptionHelper.show('call-info');
        currentScrollPosition = 0;
      });
    },
    close: function (evt) {
      console.log('cloase method was called');
      close(evt);
    }
  };
  exports.CallInfo = CallInfo;
})(window);
