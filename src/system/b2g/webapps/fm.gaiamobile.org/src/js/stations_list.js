/* exported StationsList */
'use strict';

(function(exports) {

  // StationsList Constructor
  // StationsList will be loaded only while station list shown
  var StationsList = function() {
    this.scanningAborted = false;
    this.currentFrequency = null;
    this.previousFrequency = null;
  };

  // Switch from favorite list UI to station list UI
  StationsList.prototype.switchToStationListUI = function() {
    if (StatusManager.status !== StatusManager.STATUS_FAVORITE_SHOWING) {
      // Only in favorite list UI can switch to station list UI
      return;
    }

    // Hidden frequencu dialer UI
    FMElementFrequencyBar.classList.add('hidden');
    // Change frequency list to 'stations-list' to update UI
    FMElementFrequencyListUI.className = 'stations-list';

    var stationslist = FrequencyManager.getStationsFrequencyList();
    if (stationslist.length === 0) {
      // Scan stations if no stations
      this.startScanStations();
    } else {
      // Show stations lit UI
      FrequencyList.updateStationsListUI();
      // Update StatusManager to update softkeys
      StatusManager.update(StatusManager.STATUS_STATIONS_SHOWING);
    }

    // Update current focus
    FocusManager.update();
    // Update warning UI
    // in case of favorite list warning UI is showing
    WarningUI.update();
  };

  // Switch from station list UI to favorite list UI
  StationsList.prototype.switchToFavoriteListUI = function() {
    if (StatusManager.status !== StatusManager.STATUS_STATIONS_SHOWING) {
      // Only in station list UI can switch to favorite list UI
      return;
    }

    // Show frequency dialer
    FMElementFrequencyBar.classList.remove('hidden');
    // Change frequency list to 'favorites-list' to update UI
    FMElementFrequencyListUI.className = 'favorites-list';

    // Update StatusManager to update softkeys
    StatusManager.update(StatusManager.STATUS_FAVORITE_SHOWING);

    // Show favorite list UI
    FrequencyList.updateFavoriteListUI();

    // Update current focus
    FocusManager.update();
    // Update warning UI
    // in case of favorite list warning UI is showing
    WarningUI.update();
  };

  // Start scan stations
  StationsList.prototype.startScanStations = function() {
    // Show scan progress UI
    FMElementScanProgress.className = 'throb';
    // Add 'scanning' to update stations list UI
    FMElementFrequencyListUI.classList.add('scanning');

    // Update StatusManager to update softkeys
    StatusManager.update(StatusManager.STATUS_STATIONS_SCANING);

    // clear the stations list scanned before
    this.clearAllStationsList();

    // Mark flag 'scanningAborted' as false
    this.scanningAborted = false;

    // Reset parameter 'previousFrequency' as frequencyLowerBound
    this.previousFrequency = mozFMRadio.frequencyLowerBound;
    // Change 'onfrequencychange' to 'scanResult' function
    // to handle the frequency scanned
    mozFMRadio.onfrequencychange = this.scanResult.bind(this);

    // request to scan stations
    this.requestToScanStations();
  };

  // Request to scan stations
  StationsList.prototype.requestToScanStations = function() {
    // Set frequency as 'frequencyLowerBound', whether success or failed, start scan stations
    // It is to make sure stations scanning start from the lower bound frequency
    var request = mozFMRadio.setFrequency(this.previousFrequency);
    request.onsuccess = this.continueScanStations.bind(this);
    request.onerror = this.continueScanStations.bind(this);
  };

  // clear the stations list scanned before
  StationsList.prototype.clearAllStationsList = function() {
    FrequencyList.clearCurrentFrequencyList();
    FrequencyManager.clearAllStationsFrequencyList();
  };

  // Add frequency scanned to stations list UI
  StationsList.prototype.addStationScanned = function(frequency) {
    // Update current frequency to data base
    FrequencyManager.updateFrequencyStation(frequency, true);
    // Update current stations list UI
    FrequencyList.updateStationsListUI();
    // Make the frequency in frequency dialer UI is current palying frequency
    FMRadio.onFrequencyChanged();
    // Update current focus
    FocusManager.update();
  };

  // Handle the scanned frequency
  StationsList.prototype.scanResult = function() {
    // Get the frequency scanned
    this.currentFrequency = mozFMRadio.frequency;

    if (this.previousFrequency === this.currentFrequency) {
      // Continue scanning if scanned frequency has no change
      this.continueScanStations();
      return;
    }

    if (this.previousFrequency > this.currentFrequency) {
      // Scanning finished if scanned frequency is smaller
      this.scanFinished(true, 'tcl-scanning-completed');
      return;
    }

    // Update the scanned frequency to 'previousFrequency'
    this.previousFrequency = this.currentFrequency;
    // Add frequency scanned to stations list UI
    this.addStationScanned(this.currentFrequency);

    // Check if current scanning is aborted or not
    if (this.scanningAborted) {
      this.scanFinished(true);
      return;
    }

    // Continue scanning
    this.continueScanStations();
  };

  // The actually station scanning operation
  StationsList.prototype.continueScanStations = function() {
    setTimeout(function() { mozFMRadio.seekUp(); }, 100);
  };

  // Stations scanning operation finished
  StationsList.prototype.scanFinished = function(needupdate, message) {
    // Hidden scan progress UI
    FMElementScanProgress.className = 'hidden';
    // Remove 'scanning' to update stations list UI
    FMElementFrequencyListUI.classList.remove('scanning');

    // Mark flag 'scanningAborted' as false
    this.scanningAborted = false;

    // Show toast message
    FMRadio.showMessage(message);

    // Change 'onfrequencychange' back to 'onFrequencyChanged' function
    mozFMRadio.onfrequencychange = FMRadio.onFrequencyChanged.bind(FMRadio);

    // Update StatusManager to update softkeys
    StatusManager.update(StatusManager.STATUS_STATIONS_SHOWING);

    if (needupdate) {
      // Make the frequency in frequency dialer UI is current palying frequency
      FMRadio.onFrequencyChanged();
      // Update current focus
      FocusManager.update();
    }
  };

  // Abort stations scanning operation
  StationsList.prototype.abortScanStations = function(headphone) {
    // Cancel seek
    var request = mozFMRadio.cancelSeek();
    if (headphone) {
      // Abort for headphone has been unplugged
      request.onsuccess = this.scanAbortedHeadphone.bind(this);
    } else {
      // Abort for abort softkey clicked
      request.onsuccess = this.scanAbortedNormal.bind(this);
    }

    request.onerror = function() {
      this.abortScanStations(headphone);
    }.bind(this);
  };

  // Abort stations scanning operation for headphone has been unplugged
  // no need to update focus
  StationsList.prototype.scanAbortedHeadphone = function() {
    this.scanningAborted = true;
    this.scanFinished(false);
    this.switchToFavoriteListUI();
  };

  // Abort stations scanning operation for abort softkey clicked
  // need to update focus
  StationsList.prototype.scanAbortedNormal = function() {
    this.scanningAborted = true;
    this.scanFinished(true);
  };

  exports.StationsList = new StationsList();
})(window);
